'use strict';

const gulp = require('gulp')
require('./tasks/')

// Helpers
let uglify = require('gulp-uglify')
let concat = require('gulp-concat')
let size = require('gulp-size')
let fileinclude = require('gulp-file-include')

/**
 * Config. environment, etc.
 ====================================================================
 */

const isDev = (process.env.NODE_ENV != 'production') ? true : false;
!isDev && console.log(`::: run as ${process.env.NODE_ENV} :::`)

const cfg = {
  dest: 'dist/assets/',
  scripts: ['src/scripts/app.js'],
  vendors: [],
  styles: ['src/styles/app.scss'],
  html: ['src/*.html']
}

/**
 * Styles
 =========================================================================
 */

gulp.task('styles', () =>
  CSS({
    input: cfg.styles,
    output: cfg.dest,
    build: true
  })
)

/**
 * SVGs
 =========================================================================
 */

// исходники : оптимизация
gulp.task('svg:opt', done => {
  SVG({
    input: 'src/svg/opt/[^~]*.svg',
    output: 'src/svg/opt'
  })
  done()
})

// спрайты : сборка : оптимизация
gulp.task('svg:sprites', done => {
  SVGsprites({
    cwd: 'src/svg',
    input: ['sprite'],
    output: 'src/svg'
  })
  done()
})

/**
 * SVGs
 =========================================================================
 */

gulp.task('styles:images', () =>
  optimizeImages({
    input: 'src/i/[^~]*',
    output: cfg.dest + 'i'
  })
)

gulp.task('images', () =>
  optimizeImages({
    input: 'src/images/*',
    output: 'dist/images'
  })
)

/**
 * Scripts
 =========================================================================
 */

gulp.task('scripts', () => {
  Browserify({
    input: cfg.scripts,
    output: cfg.dest
  })
})

gulp.task('scripts:build', done => {
  Browserify({
    input: cfg.scripts,
    output: cfg.dest,
    sourcemaps: true,
    build: true
  })
  done()
})

/**
 * HTML
 =========================================================================
 */

gulp.task('html', done => {
  HTML({
    input: cfg.html,
    basepath: 'src/html/',
    build: true,
    output: 'dist'
  })
  done()
})

/**
 * BrowserSync
 =======================================================================
 */

let browserSync = require('browser-sync').create()
gulp.task('browser-sync', () => {
  browserSync.init({
      notify: false,
      open: false,
      server: {
        baseDir: 'dist',
        routes: {
          '/assets': './assets',
          '/images': './images',
        },
      },
      files: [
        'dist/*.html', 'dist/assets/**/*.{js|css|svg}'
      ]
    })
    // gulp.watch('./dist/assets/**/*.css').on('change', browserSync.reload)
})

/**
 * Watch
 =======================================================================
 */

// Rerun the task when a file changes
gulp.task('default', () => {
  gulp.watch('src/svg/sprite/[^~]*.svg', gulp.series('svg:sprites'))
  gulp.watch('src/styles/**/[^~]*.scss', gulp.series('styles'))
  gulp.watch('src/scripts/**/[^~]*.js', gulp.series('scripts'))
  gulp.watch('src/images/**/[^~]*', gulp.series('images'))
  gulp.watch('src/**/[^~]*.html', gulp.series('html'))
})

// Rerun with BrowserSync
gulp.task('serve', gulp.parallel('default', 'browser-sync'))

gulp.task('build', gulp.series(
  gulp.parallel('images', 'svg:sprites', 'scripts:build', 'styles'), 'html'))
