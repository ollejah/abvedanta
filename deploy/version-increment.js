'use strict'
let fs = require('fs')
let exec = require('child_process').exec

let packagePath = '../package.json'
let versionPropertiesPath = '../android/app/version.properties'
let packageInfo = require(packagePath)
let versionParts = packageInfo.version.split('.')
let buildInt = parseInt(versionParts[2]) + 1
let newVersion = [versionParts[0], versionParts[1], buildInt].join('.')

// use apple's agvtool to update info.plist version
exec(`(cd ios && agvtool new-marketing-version ${newVersion})`, function (err, stdout, stderr) {
  if (err) throw err
  if (stderr) throw err
  writeAndroid(newVersion)
  writePackage(newVersion)
})

// finds the versionName line of version.properties and updates value to the current version
function writeAndroid(version) {
  let filePath = `${__dirname}/${versionPropertiesPath}`
  console.log('VERSION', version)
  let lines = fs.readFileSync(filePath, 'utf8')
    .split(/\r?\n|\r/)
    .map(function (line) {
      if (!/\s*=\s*/i.test(line)) return line
      let lineParts = line.split('=')
      if (lineParts[0] === 'versionName') {
        return [lineParts[0], `${version}`].join('=')
      }
      return line
    })

  fs.writeFileSync(filePath, lines.join('\n'))
}

// updates package.json to the current version
function writePackage(version) {
  packageInfo.version = version
  fs.writeFileSync(__dirname + '/' + packagePath, JSON.stringify(packageInfo, null, 2))
}
