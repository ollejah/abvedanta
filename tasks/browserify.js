/**
 * Task : browserify
 ============================================================================
 */

'use strict';

// Helpers
let util = require('gulp-util')
let header = require('gulp-header')
let sourcemaps = require('gulp-sourcemaps')
let uglify = require('gulp-uglify')
let gulpif = require('gulp-if')
let size = require('gulp-size')

// Builder
let browserify = require('browserify')
let watchify = require('watchify')
let source = require('vinyl-source-stream')
let buffer = require('vinyl-buffer')

// Transforms
let stringify = require('stringify')
let babelify = require('babelify')

// Task
global.Browserify = function(opts) {

  // opts.input : 
  // если в массиве передается больше одного файла
  // browserify их импортирует в общий билд на выходе
  // если нужно собирать отдельные бандлы пофайлово
  // поставьте флаг split : true

  opts = Object.assign({
    buildname: 'app.js',
    external: ['jquery', 'lodash'],
    sourcemaps: false,
    split: false,
    build: false,
    loose: false
  }, opts)

  // babel presets by opts conf
  let presets = {
    babelrc: false, // prevent .babelrc from using local
    compact: opts.build,
  }

  if (opts.loose) {
    presets = Object.assign({
      presets: ["es2015-loose"], // loose for oldest browsers
      plugins: [
        "transform-es3-member-expression-literals",
        "transform-es3-property-literals"
      ]
    }, presets)
  } else {
    presets = Object.assign({
      presets: ["es2015"]
    }, presets)
  }

  // если нужно собирать отдельные бандлы пофайлово
  if (opts.split) {
    opts.input.forEach(file => {
      let dest = file.split('/').pop()
        .replace(/_/g, '').replace(/js/g, 'bundle.js')
      return Bandler(file, dest)
    })
  } else {
    return Bandler(opts.input, opts.buildname)
  }

  function Bandler(file, dest) {
    let _options = {
      entries: file,
      debug: false, // true for sourcemaps
      cache: {},
      packageCache: {},
      fullPaths: false
    }

    let options = Object.assign({}, watchify.args, _options)
    let bundler = browserify(options)
    let b = !opts.build ? watchify(bundler) : bundler

    b.on('update', bundle)
    b.on('log', util.log)

    // исключаем вендоров, позже в отдельный билд
    b.external(opts.external)

    // add transformations here
    // uses .babelrc
    b.transform(babelify, presets)
    b.transform(stringify, {
      // appliesTo: { includeExtensions: ['.html'] },
      minify: true,
      // https://www.npmjs.com/package/stringify#minification
      minifyOptions: {
        collapseWhitespace: true,
        removeAttributeQuotes: false
      }
    })

    function bundle() {
      b.bundle()
        .on('error', util.log.bind(util, 'Browserify Error'))
        .pipe(source(dest))
        .pipe(buffer())
        .pipe(gulpif(opts.sourcemaps, sourcemaps.init({
          loadMaps: true
        })))
        .pipe(gulpif(opts.build, uglify({
          compress: {
            drop_console: true
          }
        })))
        .pipe(gulpif(opts.sourcemaps, sourcemaps.write('./')))
        .pipe(gulpif(opts.build, header('/*! @latest: ' + util.date() + ' *\/\n')))
        .pipe(gulpif(opts.build, size({
          showFiles: true
        })))
        .pipe(gulp.dest(opts.output))
    }
    return bundle()

  }

}

