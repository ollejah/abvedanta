'use strict';

/**
 * Выгрузка файлов на сервер
 * https://github.com/morris/vinyl-ftp
 ============================================================================
 */

let ftp = require('vinyl-ftp')
let util = require('gulp-util')
let gulpif = require('gulp-if')

global.upload = opts => {
  opts = Object.assign({
    output: '',
    force: false
  }, opts);

  // config передаем из файла sftp-config.json
  var cfg = opts.config;

  var connect = ftp.create({
    host: cfg.host,
    user: cfg.user,
    password: cfg.password,
    // parallel: 10,
    log: util.log
  });

  gulp.src(opts.input, {
      // will transfer everything to /public_html correctly
      base: '.',
      // turn off buffering for best performance for large files
      buffer: false
    })
    // only upload newer files
    // .pipe(connect.newer(cfg.remote_path + opts.output))
    .pipe(gulpif(!opts.force,
      connect.newer(cfg.remote_path + opts.output)
    ))
    .pipe(connect.dest(cfg.remote_path + opts.output))
};

/*
// ftp пока deprecated поскольку не поддерживает стрим
global.fileUpload = (path, config) => {
    // config передаем из файла sftp-config.json
    path = path || '';
    // метод загрузки передаем из config.type ? ftp : sftp
    return $[config.type]({
        host: config.host,
        user: config.user,
        pass: config.password,
        remotePath: config.remote_path + path
    });
};*/

// let sftp = require('gulp-sftp')
// gulp.task('update', () =>
//     gulp.src('static/**/*')
//     .pipe(sftp({
//         host: config.host,
//         user: config.user,
//         pass: config.password,
//         remotePath: config.remote_path + 'static/'
//     }))
// )
