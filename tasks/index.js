'use strict';

// Gulp
global.gulp = require('gulp')
global.pkg = require('../package.json')

// Helpers
var util = require('gulp-util')

/**
 * Вместо того, чтобы указать каждый плагин
 * gulp-load-plugins будут искать свой packages.json файл
 * и автоматически включать их в качестве $.pluginName()
 * загрузит только те, что используются в задаче
 ==========================================================================
 */
// global.$ = require('gulp-load-plugins')({
//     pattern: ['gulp-*', 'gulp.*'],
//     replaceString: /\bgulp[\-.]/,
//     camelize: true
// })

/**
 * Если случается ошибка при работе галпа, 
 * воспроизводится звук
 ==========================================================================
 */
global.onError = (error) => {
  util.beep();
  util.log(util.colors.red.inverse('ERROR: ' + error.message));
  util.log(util.colors.red('File: ' + error.fileName + '; Line: ' + error.lineNumber));
}

/**
 * Tasks
 * загружаем задачи помодульно
 ==========================================================================
 */
var path = require('path').join(__dirname, '.');
require('fs').readdirSync(path).forEach(function(file) {
  if (file.match(/\.js$/) !== null && file !== 'index.js') {
    require('./' + file)
  }
})
