'use strict';

/**
 * IMAGES
 * Оптимизация изображений
 * Минификация изображений
 * npmjs.com/package/gulp-imagemin
 *
 * TODO : спрайты???
 ============================================================================
 */

let imagemin = require('gulp-imagemin')
let pngcrush = require('imagemin-pngcrush')
let plumber = require('gulp-plumber')
// let changed = require('gulp-changed')
let cached = require('gulp-cached')
let size = require('gulp-size')

var fileImagemin = files => {
    return imagemin({
        // png Select an optimization level between 0 and 7.
        // optimizationLevel: 5,
        progressive: true,
        use: [pngcrush()]
    })
}

global.optimizeImages = opts =>
    gulp.src(opts.input)
    .pipe(plumber({
        errorHandler: onError
    }))
    // .pipe(changed(opts.output))
    .pipe(cached(opts.output))
    .pipe(fileImagemin())
    .pipe(size({
        showFiles: true
    }))
    .pipe(gulp.dest(opts.output))
