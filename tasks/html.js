'use strict';

/**
 * HTML
 * Оптимизация html
 * ignored by wrapping them with <!-- htmlmin:ignore -->
 * https://github.com/kangax/html-minifier
 * https://github.com/jonschlinkert/gulp-htmlmin
  ============================================================================
 */

let util = require('gulp-util')
let fileinclude = require('gulp-file-include')
let inline = require('gulp-inline')
let htmlmin = require('gulp-htmlmin')
let plumber = require('gulp-plumber')
let gulpif = require('gulp-if')
let header = require('gulp-header')
let size = require('gulp-size')
let rename = require("gulp-rename")
let replace = require("gulp-replace")

// Options
const htmlminOptions = {
  removeComments: true,
  removeCommentsFromCDATA: true,
  removeCDATASectionsFromCDATA: true,
  collapseWhitespace: true,
  conservativeCollapse: false,
  collapseBooleanAttributes: true,
  //removeAttributeQuotes: true,
  removeRedundantAttributes: false,
  useShortDoctype: true,
  removeEmptyAttributes: true,
  removeOptionalTags: true,
  //removeEmptyElements: true,
  //lint: true,
  keepClosingSlash: true,
  caseSensitive: true,
  minifyJS: true,
  minifyCSS: true,
  minifyURLs: true,
  //ignoreCustomComments: true,
  //processScripts: true,
  //customAttrAssign: true,
  //customAttrSurround: true
};

/**
 * HTML build or include
 * based o options
 * inlened & minimized
 */
global.HTML = opts => {
  opts = Object.assign({
    basepath: '.',
    build: false,
    inline: false
  }, opts);

  gulp.src(opts.input)
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(fileinclude({
      prefix: '@',
      basepath: opts.basepath
    }))
    .pipe(gulpif(opts.inline,
      inline({
        base: '.',
        disabledTypes: ['svg', 'img']
      })
    ))
    .pipe(gulpif(opts.build,
      htmlmin(htmlminOptions)
    ))
    .pipe(gulpif(opts.build,
      header('<!-- @latest: ' + util.date() + ' @author: <ollejah@gmail.com> -->\n')
    ))
    .pipe(gulpif(opts.build,
      replace('<buildstamp>', Date.now())
    ))
    .pipe(size({
      showFiles: true
    }))
    .pipe(gulpif(opts.rename,
      rename(opts.rename)
    ))
    .pipe(gulp.dest(opts.output));
};

/**
 * Оптимизируем HTML
 */
// global.compileStaticHTML = (opts) => {
//     opts = extend({
//         rename: 'index.html',
//     }, opts);

//     return gulp.src(opts.input)
//         .pipe($.plumber({
//             errorHandler: onError
//         }))
//         .pipe($.htmlmin(htmlminOptions))
//         .pipe(fileSize())
//         .pipe($.rename(opts.rename))
//         .pipe(gulp.dest(opts.output));
// };

/**
 * Извлекает, конкатенирует, обрабатывает стили и скрипты из html
 * https://www.npmjs.com/package/gulp-useref
 * TODO: допилить, чтобы принимал масив файлов
 */
// global.extractAssetsHTML = (opts) => {
//     opts = extend({
//         output: './tmp',
//     }, opts);

//     return gulp.src(opts.input)
//         .pipe($.plumber({
//             errorHandler: onError
//         }))
//         .pipe($.useref())
//         .pipe($.if('*.css', optimizeCSS()))
//         .pipe($.if('*.js', $.uglify()))
//         .pipe(gulp.dest(opts.output));
// };

/**
 * Встраивает извлеченные стили и скрипты в html
 */
// global.embedAssetsHTML = (opts) => {
//     return gulp.src(opts.input)
//         .pipe($.changed(opts.output))
//         .pipe($.plumber({
//             errorHandler: onError
//         }))
//         .pipe($.replace('<link rel="stylesheet" href="tmp.css">', () => {
//             return '<style>' + getFileContent('./tmp/tmp.css') + '</style>';
//         }))
//         .pipe($.replace('<script src="tmp.js"></script>', () => {
//             return '<style>' + getFileContent('./tmp/tmp.js') + '</style>';
//         }))
//         .pipe($.if('*.html', $.htmlmin(htmlminOptions)))
//         .pipe(fileModifiedHTML())
//         .pipe(fileSize())
//         .pipe(gulp.dest(opts.output))
//         .on('end', () => {
//             fileLog('HTML assets embed complete');
//             cleaner('./tmp/');
//         });
// };

