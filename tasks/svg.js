'use strict';

/**
 * SVG
 * Оптимизация svg
  ========================================================================
 */

// github.com/svg/svgo
// github.com/ben-eb/gulp-svgmin
let svgmin = require('gulp-svgmin')
let svgSprite = require('gulp-svg-sprite')
let size = require('gulp-size')
let cached = require('gulp-cached')
let plumber = require('gulp-plumber')

global.SVG = function(opts) {
  opts = Object.assign({
    rename: false
  }, opts)

  return gulp.src(opts.input)
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(cached('svg'))
    .pipe(svgmin({
      plugins: [{
        removeViewBox: false
      }, {
        removeUselessStrokeAndFill: true
      }, {
        removeEmptyAttrs: true
      }, {
        cleanupIDs: true
      }, {
        cleanupNumericValues: {
          floatPrecision: 2
        }
      }, {
        removeTitle: true
      }, {
        removeDesc: true
      }]
    }))
    .pipe(gulp.dest(opts.output))
};

/**
 * SVG icons and sprites
 * configurator: http://jkphl.github.io/svg-sprite/#gulp
 * github.com/jkphl/gulp-svg-sprite
 * medium.com/@iamryanyu/svg-sprite-workflow-that-works-f5609d4d6144#.ffzcwn6a4
 */

global.SVGsprites = function(opts) {
  return new Promise((resolve, reject) => {
    opts.input.forEach(folder => {
      gulp.src(folder + '/*.svg', { cwd: opts.cwd })
        .pipe(svgSprite({
          // transform: [{
          //   svgo: {
          //     plugins: [{
          //       removeViewBox: true
          //     }]
          //   }
          // }],
          svg: {
            namespaceClassnames: false,
            xmlDeclaration: false,
            doctypeDeclaration: false,
            namespaceIDs: false,
            dimensionAttributes: false,
            // rootAttributes: {
            //     viewBox: ''
            // }
            transform: [
              (svg) => {
                svg = svg.replace(/^<svg viewBox="(.*?)"/g, '<svg')
                return svg
              }
            ]
          },
          mode: {
            stack: {
              example: false,
              dest: '.',
              sprite: folder + '.svg'
            }
          }
        }))
        .pipe(size({
          showFiles: true
        }))
        .pipe(gulp.dest(opts.output))
      })
      resolve()
    })
    .then(() => console.log('Спрайты готовы'))
};
