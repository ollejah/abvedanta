/**
 * CSS и SASS
 * Компиляция и оптимизация стилей
 =========================================================================
 */

'use strict';

// Helpers
let cleanCSS = require('gulp-clean-css')
let gulpif = require('gulp-if')
let header = require('gulp-header')
let pleeease = require('gulp-pleeease')
let plumber = require('gulp-plumber')
let sass = require('gulp-sass')
let size = require('gulp-size')
let sourcemaps = require('gulp-sourcemaps')
let util = require('gulp-util')

// http://pleeease.io/docs/
const pleaseOptions = {
  autoprefixer: ['last 5 versions'],
  // autoprefixer: { browsers: ['ie 9'] },
  filters: true,
  import: true,
  minifier: {
    removeAllComments: true,
    preserveHacks: true
  },
  mqpacker: true,
  next: false,
  opacity: false,
  pseudoElements: true,
  // rem: ['16px', {
  //   replace: true
  // }],
  rem: false,
  sourcemaps: {
    inline: false
  }
}

global.CSS = function(opts) {
  opts = Object.assign({
    // sourcemaps: false,
    build: true,
    root: '',
    sourceRoot: '.',
    destPath: '.'
  }, opts)

  return gulp.src(opts.input)
    .pipe(plumber({
      errorHandler: onError
    }))
    // https://www.npmjs.com/package/gulp-sourcemaps
    .pipe(gulpif(opts.build, sourcemaps.init({
      loadMaps: true,
      debug: true
    })))
    .pipe(gulpif(['*.scss', '*.sass'], sass()))

  // пути к картинкам относительно корня проекта
  // .pipe($.cssUrlAdjuster({
  //     // prepend: opts.root
  //     prependRelative: opts.root,
  //     replace: opts.replace // ['/old/path','/brand/new']
  // }))

  .pipe(pleeease(pleaseOptions))
    .pipe(cleanCSS())
    .pipe(gulpif(opts.build, header('/*! @latest: ' + util.date() + ' *\/\n')))
    .pipe(gulpif(opts.build, sourcemaps.write(opts.destPath, {
      includeContent: true,
      sourceRoot: opts.sourceRoot
    })))
    .pipe(gulpif(opts.build, size({
      showFiles: true
    })))
    .pipe(gulp.dest(opts.output))
    // .pipe(reload({ stream: true }))
}

