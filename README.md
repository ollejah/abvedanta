# Абведанта

[http://abvedanta.ru](abvedanta.ru)

**Install node, yarn (optional), dependencies...**

[https://yarnpkg.com/en/docs/install]()

Можно юзать классчический npm, заменить команды yarn на npm

```
cd <project>
yarn install
```

**Запускаем сборку фронтенда с отслеживанием изменений**

```
yarn run dev
```

Идем на [http://localhost:3000]()

**Билдим продакшен**

```
yarn run build
```

**Release**

```
yarn run release
```
