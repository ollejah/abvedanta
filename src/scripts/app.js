'use strict';

import '../vendors/svgxuse.min'
import { touchAction } from './helpers/h.touch-action'
touchAction()

/**
 * Dom helpers
 */

window.qs = document.querySelector.bind(document)
window.qsa = document.querySelectorAll.bind(document)
Element.prototype.on = Element.prototype.addEventListener

/**
 * Toggles
 */
const $html = qs('html')
const $modal = qs('.js-navigation')
const $pointer = qs('.js-hamburger')

function toggleModal(e) {
  e.preventDefault()
  $html.classList.toggle('is-disabled')
  $pointer.classList.toggle('is-active')
  $modal.classList.toggle('is-active')
  document.addEventListener('keydown', e => {
    if ($html.classList.contains('is-disabled') && e.keyCode == 27) {
      toggleModal(e)
    }
  })
}

// Hamburger toggle
qs('.js-hamburger').on('click', e => {
  toggleModal(e)
  $modal.classList.toggle('-is-menu')
  if ($modal.classList.contains('-is-search')) {
    $modal.classList.remove('-is-search')
    $modal.classList.remove('-is-menu')
  }
})

// Search toggle
qs('.js-search').on('click', e => {
  toggleModal(e)
  $modal.classList.toggle('-is-search')
  if ($modal.classList.contains('-is-menu')) {
    $modal.classList.remove('-is-menu')
  }
})

/**
 * Frontpage Carousel
 */

// var buttons = document.querySelectorAll('.carousel-dots span');
// var carousel = document.querySelector('.slides');
// [].forEach.call(buttons, function (button, i) {
//   button.addEventListener('click', function (e) {
//     let slide = document.querySelector('#' + e.target.getAttribute('link'))
//     carousel.style.transform = "translateX(-" + slide.offsetLeft + "px)";
//   })
// })

/**
 * Strict valid target="blank" attribute
 */

qs('a[href^="http"]:not([href*="' + location.host + '"])').setAttribute('target', '_blank')

/**
 * Вешаем клик на весь блок, содержащий ссылку
 * @param  {DOM} pointer элемент, инициирущий переход
 * @param  {String} children дочерний элемент, где искать ссылку
 * @use clickArea('.b-discount__item');
 */
const clickArea = function (pointer, children) {
  const item = pointer.querySelector(children)
  item && pointer.on('click', () => {
    if (item.href) window.location.href = item.href
    return false
  })
}
const $cards = qsa('.c-card');
[].forEach.call($cards, item => clickArea(item, 'h2 a'))
